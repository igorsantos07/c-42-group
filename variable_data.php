<?php
    $bitbucket = [
        'calen-do' => 'https://bitbucket.org/santos_dal/calen-do',
        'c42' => 'https://bitbucket.org/santos_dal/c-42-group'
    ];

    $calendo = 'http://calendo-igorsantos07.dotcloud.com';

    $members = [
        'Abdurrahman Gattous' => [
            'role' => 'Developer',
            'email' => 'gattous@cs.dal.ca',
            'description' => "Abdurrahman is the team's User-Interface and Experience expert.
				He develops the program to cater to the target audience's needs and requirements.
				He has designed the interface from ground-up in coherence with Tigra's aims and values.
				Abdurrahman has ensured that Tigra remains as robust as possible with-out any bloating or wasted space.
				The mobile web-app is ensured to be ideal for all platforms and screen orientations."
        ],

        'Hamza Ayaz' => [
            'role' => 'Project Leader',
            'email' => 'hamza.ayaz@dal.ca',
            'description' => "As project manager, Hamza aims to get the best out of an already excellent team. Hamza is
                responsible for clearing the path through all scheduling, organization and documentation so that the TSE
                team can focus on what they do best: Developing powerhouse applications.</p><p>
                Hamza is a Java programmer who has lead software development projects for 2 years. He has modeled a
                real-world business environment using SQL and designed websites using JavaScript, jQuery, HTML5, CSS
                languages."
        ],

        'Igor Santos' => [
            'role' => 'SysAdmin / SysOps',
            'email' => 'santos@cs.dal.ca',
            'description' => "Igor's main role is to maintain the company website (this page) and he is
                    the lead developer of the <a href='$tigra'>Tigra Project</a>. He's also studying about Continuous
                    Integration with Jenkins, and soon we will also have a CI Build Server up and running.</p><p>
                    Igor is web developer for 5 years and have some experience with different areas of
                    programming, such as code development, PHP and Ruby frameworks and environments, SVN/Git, and
                    SQL/NoSQL databases."
        ],

        'Richard Della' => [
            'role' => 'Test Guru',
            'email' => 'richdellon@gmail.com',
            'description' => "Desc about Richard's role and background"
        ],

		'Luís Guadagnin' => [
			'role' => 'Front-end Developer',
			'email' => 'guadagnin@cs.dal.ca',
			'description' => "Desc about Luís's role and background"
		],

		'Derek Neil' => [
			'role' => 'Back-end Developer',
			'email' => 'dneil@cs.dal.ca',
			'description' => "Desc about Derek's role and background"
		],

		'Raissa Giorizzatto' => [
			'role' => 'UI/UX Developer',
			'email' => 'giorizzatto@cs.dal.ca',
			'description' => "Desc about Raissa's role and background"
		],

		'Justin Frank' => [
			'role' => 'Test Guru',
			'email' => 'jfrank@cs.dal.ca',
			'description' => "Desc about Justin's role and background"
		],

    ];

	ksort($members);
