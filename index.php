<? require 'variable_data.php' ?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <title>C-42 Group</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/pygment_trac.css">
    <script src="js/scale.fix.js"></script>
    <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <? include "js/ga.php" ?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
	<script>
		function changeSection() {
			$('section').hide(0, function() {
				switch (window.location.hash) {
					case '#the-team':	$('section#the-team').show(); break
					case '#meetings':	$('section#meetings').show(); break
					case '#project':
					default:			$('section#project').show(); break
				}
			})
		}

		$(window)
			.on('hashchange', changeSection)
			.one('load', changeSection)
	</script>
</head>

<body>
<div class="wrapper">
<header>
    <h1><img src="images/logo_c42.png" /></h1>
    <p id="desc">
        <strong>C-42 Group</strong>, the company behind <a href="<?=$calendo?>">Calen-Do Project</a>,
        for the <a href="http://web.cs.dal.ca/~arc/teaching/CS3130/2013w/projectDescription.html">3130 Software
        Engineering course</a>.
    </p>


    <ul id="buttons">
        <li class="download">
            <a class="buttons" href="<?=$bitbucket['calen-do']?>/get/master.zip">Download ZIP</a>
        </li>
        <li>
            <a class="buttons bitbucket" href="<?=$bitbucket['calen-do']?>">See on BitBucket</a>
        </li>
    </ul>

    <div id="pages">
        <h2>Company details</h2>
        <ul class="pages">
            <li><a href="#project">Our Projects</a></li>
            <li><a href="#the-team">The Team</a></li>
            <li><a href="#meetings">The Meetings</a></li>
        </ul>
    </div>

    <div id="team">
        <h2>Team</h2>
        <dl class="members">
            <? foreach ($members as $name => $data): ?>
                <dt><a class="name" href="mailto:<?=$data['email']?>"><?=$name?></a>
                <!--dd><?=$data['role']?></dd-->
            <? endforeach ?>
        </dl>
    </div>
</header>

<section id="project">
    <h1>Our Project</h1>
    <p>
        C-42 Group is a fast-paced organization with a heavy focus on producing
        the ideal product for the user. With this philosophy in mind, C-42 began a period of research and
        analysis in order to discover a challenge which a large proportion of the population faces and
        then to engineer a solution to that problem.
    </p>
    <p>
        We know that <a href="http://www.pewinternet.org/~/media/Files/Reports/2008/PIP_Networked_Workers_FINAL.pdf.pdf"
        title="PEW RESEARCH CENTER’S INTERNET & AMERICAN LIFE PROJECT. (2008). (pg 5) Most workers use the internet or
        email at their jobs, but they say these technologies are a mixed blessing for them.">50% of users are expected
        to complete work tasks while on leisure time</a>, and <a href="http://www.phwa.org/dl/2010phwp_fact_sheet.pdf"
        title="American Psychological Association Practice Organization. (2010). Psychologically Healthy Workplace
        Program Fact Sheet: By the Numbers. (pg 1)">51% of workers have reduced productivity due to stress</a>. At C-42,
        we decided to lend a helping hand. We believe that an effective Task Management System can help users reduce
        stress, their sense of being overwhelmed, and can increase their desire to be more productive by clarifying the
        true overall picture of their tasks.
    </p>
    <p style="font-weight: bold">Our mission is to give the user their ability to think deeply back!</p>
    <p>
        The C-42 team began to discuss various solutions to support this mission, all based around a
        Task List Management System concept. We are currently developing the <strong>Calen-Do System</strong>.
    </p>

    <h2>Calen-Do <em>GTD Life Management Tool</em></h2>
    <p>
        Calen-Do is a Task Management System based on the <a href="http://en.wikipedia.org/wiki/Getting_Things_Done">
        Getting Things Done methodology</a>. Developed by <a href="http://www.davidco.com/about-us/about-david-allen">
        David Allen</a>, the GTD methodology has been used by countless high profile users in order to
        streamline their lives and their tasks. Calen-Do is modeled on that idea, and gives users peace of mind by
        presenting a clean User Interface where users can comfortably manipulate their tasks, creating listings
        for their life-areas, and use a prioritization system to decide for themselves what is really important to them,
        at any given moment. <a href="http://www.youtube.com/watch?v=JhmZhTXN4as" class="youtube">See a quick video about GTD</a>.
    </p>

    <iframe class="youtube" src="http://www.youtube.com/embed/JhmZhTXN4as?rel=0" frameborder="0" allowfullscreen></iframe>

    <h3>Scope</h3>
    <p>
        Calen-Do will use various features to provide a comprehensive Task Management System to a large
        population of users. C-42 recognizes that today’s user is frequently on the go, and would like to
        be able to access their tasks while on the commute. To fulfill this requirement, C-42 developed
        Calen-Do to be web-accessible and mobile. Users visiting the webapp will be able to open, review,
        and manipulate their tasks. Here you can see how the main interface looks like on a full-blown
		desktop and a small phone's screen.
    </p>
    <img class="ui" src="images/main_ui.jpg" alt="Main UI on the computer" />
    <img class="ui" src="images/main_ui_phone.jpg" alt="Main UI on a small phone screen" />
</section>

<section id="the-team">
    <h1>The Team</h1>
    <p>
        We pride ourselves on our success
        in merging our diverse strengths and skillsets together to accomplish technically complex
        projects which require a great deal of ingenuity and creativity. Using the varied backgrounds
        and expertise found within our organization, C-42's staff are able to conduct comprehensive
        requirements and risk analysis investigations in order to provide our clients with the most
        effective and economic solution to their business problems.
    </p>
    <p>
        C-42's company culture promotes growth of staff expertise, client
        engagement, and thinking big. We are actively seeking teammates to join C-42 to develop Calen-Do
        from its current prototyping stage to its deployment on the market. If you are a problem solver
        who loves to work in teams, think in broad horizons, and prefer working under defined roles and
        responsibilities and schedules, C-42 Group is right for you.
    </p>

    <? foreach ($members as $name => $data): ?>
        <h2><?=$name?> <em class="email"><?=$data['email']?></em></h2>
        <p><strong><?=$data['role']?></strong>. <?=$data['description']?></p>
    <? endforeach ?>
</section>

<section id="meetings">
    <h1><a name="meetings"></a>The Meetings</h1>
    <p>
        We hold good discussions about the most different aspects of Calen-Do in a Google Group. that's unfortunately
        private, for the sake of our project's privacy.
    </p>
    <p>
        We also have frequent meetings. Our <a href="https://drive.google.com/#folders/0B2jbxC5Se3TBTllsUlNIczBPY0U">
        minutes are hosted on Google Drive</a>, to be easily accessible to all the team. However, the folder is also
        not accessible, to maintain the confidentiality of our decisions in those first steps. The minutes will also be
        accessible here:
        <ol>
            <li><a href="meetings/Tigra%20Meeting%20Jan 10.pdf">January 10<sup>th</sup></a></li>
            <li><a href="meetings/Tigra%20Meeting%20Jan 17.pdf">January 17<sup>th</sup></a></li>
            <li><a href="meetings/Tigra%20Meeting%20Jan 24.pdf">January 24<sup>th</sup></a></li>
            <li>January 29<sup>th</sup> - <em>we had an informal brainstorm for names and features,
                and had no minute.</em>
            </li>
        </ol>
    </p>
</section>

<footer>
    <p><small>
        Hosted on <a href="http://www.dotcloud.com">DotCloud</a>.<br />
        Using the <abbr title="Creative Commons Attribution Licensed">CC-BY</abbr> <a href="https://github.com/broccolini/dinky">Dinky theme</a>.<br />
        This page is also on <a href="<?=$bitbucket['c42']?>">BitBucket</a>.
    </small></p>
</footer>
</div>
<!--[if !IE]><script>fixScale(document);</script><!--<![endif]-->
</body>
</html>